﻿using Sirenix.OdinInspector;
using UnityEngine;

[CreateAssetMenu(fileName = "Weapon", menuName = "Weapons")]
public class WeaponData : ScriptableObject
{
    public string WeaponName;

    [Header("Attributes")]
    [SerializeField, MinValue(0f)]
    public float MovementSpeedModifier;

    [Header("Weapon")]
    [PreviewField]
    public GameObject WeaponPrefab;

    [SerializeField, MinValue(0f)]
    public float Range;

    [SerializeField, MinValue(0f)]
    public int AttackDamageModifier;

    [SerializeField, MinValue(0f)]
    public float AttackTimeDuration;

    [SerializeField, MinValue(0f)]
    public float Cooldown;

    [PreviewField]
    [SerializeField]
    public GameObject SlashEffect;
}