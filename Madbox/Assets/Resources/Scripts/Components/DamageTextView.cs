using DG.Tweening;
using Lean.Pool;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DamageTextView : MonoBehaviour
{
    [SerializeField, MinValue(0f)]
    private float animationTime;

    private TextMeshPro textMeshPro;

	private void Awake()
	{
		textMeshPro = GetComponent<TextMeshPro>();
	}

    public void SetText(string text)
    {
        textMeshPro.SetText(text);
    }

    public void SetColor(Color color, float time)
    {
        textMeshPro.DOColor(color, time);
    }

    [Button]
    public void AnimateView()
    {
        var direction = Vector3.up;

        transform.DOMove(gameObject.transform.position + direction, animationTime);
        textMeshPro.DOFade(0f, animationTime).OnComplete(() =>
        {
            textMeshPro.DOFade(1f, 0f);
            textMeshPro.DOColor(Color.white, 0f);

            LeanPool.Despawn(this);
        });
    }
}
