using Drawing;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProximitySensor : MonoBehaviour
{
	[SerializeField]
	private Vector3 offset;

	[SerializeField]
	private float radius = 1f;

	[SerializeField]
	private LayerMask layerMask;

	[SerializeField]
	[ReadOnly]
	private Collider[] targets;

	private Vector3 position;

	private void Update()
	{
		position = gameObject.transform.position + offset;
		Draw.WireSphere(position, radius);
	}

	private void FixedUpdate()
	{
		targets = Physics.OverlapSphere(position, radius, layerMask);
	}

	public Collider[] GetTargets()
	{
		return targets;
	}

	public void SetRadius(float range)
	{
		radius = range;
	}
}
