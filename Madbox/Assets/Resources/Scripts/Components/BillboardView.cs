using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardView : MonoBehaviour
{
    [SerializeField]
    private bool rotationReverse;

    private new Camera camera;

	private void Awake()
	{
        camera = Camera.main;
    }

    private void LateUpdate()
    {
        Vector3 reverse = Vector3.back;

        if (rotationReverse)
            reverse = Vector3.forward;

        transform.LookAt(transform.position + camera.transform.rotation * reverse, camera.transform.rotation * Vector3.up);
    }
}
