using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthView : MonoBehaviour
{
    [Header("References")]
    [SerializeField, Required]
	private Image imagefill;

    [Header("Setup")]
    [SerializeField]
    private Gradient gradient;

    [SerializeField, MinValue(0f), MaxValue(1f), OnValueChanged("FillAmountChanged")]
    private float fillAmount;
   
    [Button]
    public void SetFillAmount(float percentage)
    {
        DOVirtual.Float(fillAmount, percentage * 0.01f, 0.2f, (float p) =>
        {
            fillAmount = p;
            FillAmountChanged();
        });
    }

    private void FillAmountChanged()
    {
        imagefill.fillAmount = fillAmount;
        imagefill.color = gradient.Evaluate(fillAmount);
    }
}
