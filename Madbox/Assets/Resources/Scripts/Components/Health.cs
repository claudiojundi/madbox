using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public interface IDamage
{
	bool IsAlive();
	void TakeDamage(int damage, GameObject? damageEffect = null);
}

[System.Serializable]
public class DamageEvent : UnityEvent<int> { }

[System.Serializable]
public class HealthPercentageEvent : UnityEvent<float> { }

public class Health : MonoBehaviour
{
	public bool Dead = false;

	[SerializeField]
	[MinValue(0f)]
	private int maxHealth;

	[SerializeField]
	[MinValue(0f)]
	private int currentHealth;

	public DamageEvent damageEvent;
	public HealthPercentageEvent healthPercentageEvent;

	public delegate void DeathEvent();
	public DeathEvent OnDeathEvent;

	private void Start()
	{
		Dead = false;
		currentHealth = maxHealth;
	}

	[Button]
	public void TakeDamage(int damage)
	{
		currentHealth -= damage;

		damageEvent?.Invoke(damage);
		healthPercentageEvent?.Invoke((currentHealth * 100) / maxHealth);

		if (currentHealth <= 0)
		{
			Dead = true;

			OnDeathEvent?.Invoke();
		}
	}
}