using Lean.Pool;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextInstantiator : MonoBehaviour
{
	[Required]
	[SerializeField]
	private GameObject textPrefab;

	[SerializeField]
	private Vector3 offset;

	public void SpawnTextFromInt(int value)
	{
		Spawn(value.ToString());
	}

	[Button]
   public void Spawn(string text)
	{
		var textView = LeanPool.Spawn(textPrefab, gameObject.transform.position + offset, Quaternion.identity);

		var damageTextView = textView.GetComponent<DamageTextView>(); //Extends if necessary to other types of texts.

		damageTextView.SetText(text);
		damageTextView.AnimateView();
	}
}
