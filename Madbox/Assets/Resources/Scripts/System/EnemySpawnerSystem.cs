using Drawing;
using Lean.Pool;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnerSystem : MonoBehaviourGizmos
{
	[SerializeField]
	private bool canSpawn;

	[SerializeField, MinValue(0f)]
	private float rate;

	[SerializeField]
	private Vector3 boundary = Vector3.one;

	[SerializeField, Required]
	private GameObject enemyPrefab;


	//Showing how I would do difficulty level.
	[SerializeField]
	private AnimationCurve levelCurve;


	private void Start()
	{
		if (canSpawn)
		{
			InvokeRepeating("Spawn", 0f, rate);
		}
	}

	public override void DrawGizmos()
	{
		DrawSpawnableArea();
	}

	private void DrawSpawnableArea()
	{
		Draw.WireBox(gameObject.transform.position, boundary * 2f);
	}

	[Button]
	private void Spawn()
	{
		var randomX = UnityEngine.Random.Range(-boundary.x, boundary.x);
		var randomZ = UnityEngine.Random.Range(-boundary.z, boundary.z);

		var x = gameObject.transform.position.x + randomX;
		var z = gameObject.transform.position.z + randomZ;

		LeanPool.Spawn(enemyPrefab, new Vector3(x, 0f, z), Quaternion.identity);
	}
}
