using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(PlayerMovementController))]
[RequireComponent(typeof(PlayerAnimationController))]
[RequireComponent(typeof(PlayerAttackController))]
[RequireComponent(typeof(ProximitySensor))]
public class PlayerController : MonoBehaviour
{
	[Header("Setup")]
	[Required]
	[InlineEditor]
	[SerializeField]
	private JoystickValue joystickValue;

	[InlineEditor]
	[SerializeField]
	private List<WeaponData> initialWeaponsList;

	[Header("References")]
	[Required]
	[SerializeField]
	private Transform weaponContainer;

	private PlayerMovementController playerMovementController;
	private PlayerAnimationController playerAnimationController;
	private PlayerAttackController playerAttackController;
	private ProximitySensor proximitySensor;

	//Privates
	private bool attacking;
	private WeaponData currentWeaponData;

	private void Awake()
	{
		SetReferences();
	}

	private void SetReferences()
	{
		playerMovementController = GetComponent<PlayerMovementController>();
		playerAnimationController = GetComponent<PlayerAnimationController>();
		playerAttackController = GetComponent<PlayerAttackController>();
		proximitySensor = GetComponent<ProximitySensor>();
	}

	private void Start()
	{
		EquipRandomWeaponFromList();
	}

	[PropertySpace(15)]
	[Button]
	private void EquipRandomWeaponFromList()
	{
		if (initialWeaponsList.Count > 1)
		{
			var randomWeaponIndex = UnityEngine.Random.Range(0, initialWeaponsList.Count);
			EquipWeapon(initialWeaponsList[randomWeaponIndex]);
		}
	}

	private void Update()
	{
		Movement();
		Attack();
	}

	private void Movement()
	{
		if (joystickValue)
		{
			var movement = new Vector3(joystickValue.Horizontal, 0f, joystickValue.Vertical);

			if (movement.sqrMagnitude > 0.1f)
			{
				playerMovementController.Turning(movement.x, movement.z);
			}

			playerAnimationController.Move(movement.magnitude);
			playerMovementController.Move(movement, ()=>
			{
				attacking = false;
			});
		}
	}

	private void Attack()
	{
		if (proximitySensor.GetTargets().Length <= 0)
			return;

		if (!playerMovementController.IsMoving 
			&& playerAttackController.CanAttack
			&& !playerMovementController.IsDashing
			&& !attacking)
		{
			attacking = true;

			var targets = playerAttackController.ChooseTarget(proximitySensor.GetTargets());

			if (targets.Count == 0)
			{
				attacking = false;
				return;
			}
				
			playerMovementController.DashTo(targets[0].transform.position, () =>
			{

			}, () =>
			{
				playerAnimationController.Attack(currentWeaponData.AttackTimeDuration, ()=>
				{
					targets[0].GetComponent<IDamage>()?.TakeDamage(currentWeaponData.AttackDamageModifier, currentWeaponData.SlashEffect);

				}, ()=>
				{
					playerAttackController.Attack();
					attacking = false;
				});
			});
		}
	}

	[Button]
	public void EquipWeapon(WeaponData weaponData)
	{
		if (weaponData)
		{
			currentWeaponData = weaponData;

			playerAttackController.SetCooldown(weaponData.Cooldown);
			playerMovementController.SetSpeed(weaponData.MovementSpeedModifier);
			proximitySensor.SetRadius(weaponData.Range);

			if (weaponContainer.transform.childCount > 0)
			{
				Destroy(weaponContainer.transform.GetChild(0).gameObject);
			}

			Instantiate(weaponData.WeaponPrefab, weaponContainer);
		}
	}
}
