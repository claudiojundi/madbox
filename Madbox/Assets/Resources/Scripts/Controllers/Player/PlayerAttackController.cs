using DG.Tweening;
using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PlayerAttackController : MonoBehaviour
{
	public bool CanAttack = true;


	private float cooldown;

	[SerializeField, ReadOnly]
	private float currentCooldownTime;

	public enum Strategy
	{
		Closest,
		Random
	}

	[SerializeField]
	private Strategy strategy;

	public void SetCooldown(float cooldown)
	{
		this.cooldown = cooldown;
	}

	public void Attack()
	{
		if (!CanAttack)
			return;

		CanAttack = false;

		DOVirtual.Float(0f, cooldown, cooldown, (c) =>
		{
			currentCooldownTime = c;
			//UnityEngine.Debug.Log("Cooldown: " + currentCooldownTime);

		}).SetEase(Ease.Linear).OnComplete(()=>
		{
			CanAttack = true;
			currentCooldownTime = 0f;
		});
	}

	public List<Collider> ChooseTarget(Collider[] targets)
	{
		switch (strategy)
		{
			case Strategy.Closest:

				var targetsToList = targets.OrderBy(point => Vector3.Distance(transform.position, point.transform.position))
					.Where(target=> !target.GetComponent<Health>().Dead).ToList();

				return targetsToList;

			case Strategy.Random:
				//Add the logic in here.
				break;

			default:
				break;
		}

		return targets.ToList();
	}


	
}
