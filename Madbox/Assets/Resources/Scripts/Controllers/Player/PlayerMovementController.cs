using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerMovementController : MonoBehaviour
{
    [Header("Setup")]
    [SerializeField]
    private bool canMove = true;

    [Header("Settings")]
    [SerializeField,MinValue(0f)]
    private float movementSpeed = 10f;

    [SerializeField, MinValue(0f)]
    private float dashSpeed = 40f;

    [SerializeField]
    private Ease dashEaseType = Ease.Linear;

    public bool IsMoving;
    public bool IsDashing;

    public UnityEvent OnDashStart;
    public UnityEvent OnDashEnd;

    //Privates
    private Tweener dashTween;
    private float oldAngle, currentAngle;

    public void Move(Vector3 movePos, Action onMoved = null)
    {
        if (canMove)
        {
            IsMoving = movePos.sqrMagnitude > 0;

            if (IsMoving && IsDashing)
			{
                StopDash();
			}

            var playerPosition = gameObject.transform.position;
            gameObject.transform.position = Vector3.Lerp(playerPosition, playerPosition + movePos, Time.deltaTime * movementSpeed);
        }
		else
		{
            IsMoving = false;
        }
    }

    public void Turning(float h, float v)
    {
        if (currentAngle != 0 && currentAngle != -180)
        {
            oldAngle = currentAngle;
        }

        currentAngle = Angle(new Vector2(h, v));

        if (h != 0 || v != 0)
        {
            gameObject.transform.eulerAngles = new Vector3(0, currentAngle, 0);
        }
        else if (h == 0 && v == 0)
        {
            gameObject.transform.eulerAngles = new Vector3(0, oldAngle, 0);
        }
    }

    private float Angle(Vector2 p_vector2)
    {
        if (p_vector2.x < 0)
        {
            return 360 - (Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg * -1);
        }
        else
        {
            return Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg;
        }
    }

    public void SetSpeed(float speed)
	{
        movementSpeed = speed;
    }

	//public void Dash(float? dashStrength = null, Action dashStart = null, Action dashComplete = null)
	//{
	//    var dashValue = dashDistance;

	//    if (dashStrength != null)
	//        dashValue = (float)dashStrength;

	//    var dashPos = gameObject.transform.position + gameObject.transform.forward * dashValue;

	//    DashTo(dashPos, dashStart, dashComplete);
	//}

    [Button]
	public void DashTo(Vector3 position, Action dashStart = null, Action dashComplete = null)
	{
        if (IsDashing)
            return;

        IsDashing = true;

        dashTween = transform.DOMove(position, dashSpeed)
            .SetSpeedBased(true)
            .SetEase(dashEaseType)
            .OnStart(() =>
		{
            dashStart?.Invoke();
            OnDashStart?.Invoke();

        }).OnComplete(() =>
		{
            StartCoroutine(DelayCouroutine());
			IEnumerator DelayCouroutine()
			{
				yield return new WaitForSeconds(0.05f);

                IsDashing = false;
                dashComplete?.Invoke();
                OnDashEnd?.Invoke();
            }
		});
	}

	public void StopDash()
	{
		dashTween.Kill();
        IsDashing = false;
        OnDashEnd?.Invoke();
    }
}
