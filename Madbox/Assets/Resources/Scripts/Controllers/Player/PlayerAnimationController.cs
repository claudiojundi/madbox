using DG.Tweening;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
	[Required]
	[SerializeField]
	private Animator animator;

	private void Start()
	{
		Animator.StringToHash("sdada");
	}

	[Button]
	public void Move(float sqrMagnitude)
	{
		animator.SetFloat("Speed", sqrMagnitude);
	}

	[Button]
	public void Attack(float duration = 1f, Action onDamageAction = null, Action onComplete = null)
	{
		animator.SetTrigger("Attack");

		// Since we don't have separeted animations we use hardcoded.
		// We don't want to use animation event for multiplayer purposes.
		var damageEventTime = duration * 40f / 100f;

		DOVirtual.Float(0f, 1f, damageEventTime, (p) => { }).OnComplete(() =>
		  {  
			  onDamageAction?.Invoke();
		  });

		DOVirtual.Float(0f, 1f, duration, (p) =>
		 {
			 animator.SetFloat("Attack_Percentage", p);

		 }).SetEase(Ease.Linear).OnComplete(() =>
		 {
			 onComplete?.Invoke();
		 });
	}

	[Button]
	public void SetAttackSpeed(float multiplier)
	{
		animator.SetFloat("Attack_Speed", multiplier);
	}
}
