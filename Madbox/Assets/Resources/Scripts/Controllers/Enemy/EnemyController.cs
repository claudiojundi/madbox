using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(EnemyAnimationController))]
[RequireComponent(typeof(Health))]
public class EnemyController : MonoBehaviour, IDamage
{
    private Health health;
    private EnemyAnimationController enemyAnimationController;

	private void Awake()
	{
		SetReferences();
	}

	private void SetReferences()
	{
		health = GetComponent<Health>();
		enemyAnimationController = GetComponent<EnemyAnimationController>();
	}

	private void Start()
    {
		health.OnDeathEvent += HandleDeathEvent;
	}

	private void HandleDeathEvent()
	{
		enemyAnimationController.Die();
	}

	public void TakeDamage(int damage, GameObject damageEffect = null)
	{
		health.TakeDamage(damage);
		enemyAnimationController.TakeDamage(0.2f, damageEffect);
	}

	public bool IsAlive()
	{
		return health.Dead;
	}
}
