using DG.Tweening;
using Lean.Pool;
using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimationController : MonoBehaviour
{
	[Header("References")]
	[SerializeField, Required]
	private Animator animator;

	[SerializeField, Required]
	private SkinnedMeshRenderer[] viewsSkinnedMesh;

	[SerializeField, Required]
	private Cinemachine.CinemachineImpulseSource impulseSource;

	[SerializeField]
	private Material viewMaterial;

	[SerializeField]
	private Material blinkMaterial;

	//Privates.
	private Tweener punchScaleTween;

	// Start is called before the first frame update
	void Start()
	{

	}

	// Update is called once per frame
	void Update()
	{

	}

	[Button]
	public void TakeDamage(float duration = 0.2f, GameObject slashEffect = null)
	{
		animator.SetTrigger("TakeDamage");

		if (slashEffect != null)
		{
			LeanPool.Despawn(LeanPool.Spawn(slashEffect, transform.position + new Vector3(0f, 1.5f, 0f), Quaternion.identity), 1f);
		}

		impulseSource.GenerateImpulse();

		BlinkDamage(duration);
		PunchScaleDamage(duration);
	}

	public void Die()
	{
		animator.SetBool("Dead", true);
		animator.SetTrigger("Death");

		gameObject.transform.DOMoveY(-1.5f, 1f);
	}

	private void BlinkDamage(float blinkDuration)
	{
		StartCoroutine(BlinkMaterialCouroutine());
		IEnumerator BlinkMaterialCouroutine()
		{
			for (int skinnedMeshindex = 0; skinnedMeshindex < viewsSkinnedMesh.Length; skinnedMeshindex++)
			{
				viewsSkinnedMesh[skinnedMeshindex].material = blinkMaterial;
			}

			yield return new WaitForSecondsRealtime(blinkDuration);

			for (int skinnedMeshindex = 0; skinnedMeshindex < viewsSkinnedMesh.Length; skinnedMeshindex++)
			{
				viewsSkinnedMesh[skinnedMeshindex].material = viewMaterial;
			}
		}
	}

	private void PunchScaleDamage(float duration)
	{
		punchScaleTween.Kill(true);
		punchScaleTween = transform.DOPunchScale(Vector3.one * 0.2f, duration).OnComplete(() =>
		{
			transform.localScale = Vector3.one;
		});
	}
}
